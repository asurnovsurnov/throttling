from setuptools import setup

setup(
    name='throttling',
    version='1.1',
    packages=['throttling'],
    url='https://gitlab.com/asurnovsurnov/throttling.git',
    author='Alex Surnov',
    author_email='asurnovsurnov@gmail.com',
    description='This project contains a appoint_limit decorator that limits the frequency of calls to the function to which it is applied.'
                'The decorator allows you to set the maximum number of attempts to call a function and the time interval between them. '
                'This is useful when the API has restrictions on the number of requests per unit of time.'
                'The decorator wraps the function call in a try-except block, which allows you to handle possible exceptions that occur when executing the request. '
                'This can be useful when dealing with unreliable or temporarily unavailable APIs, where retries can help ensure the request succeeds.'
                'If an error occurs and the number of attempts does not exceed the set limit, the decorator pauses and re-calls the function.',
)