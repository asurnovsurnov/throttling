import asyncio
import functools
from datetime import datetime, timedelta


class AppointLimitException(Exception):
    """
        Exception that is thrown when executing the limit call function in the decorator appoint_limit.

        Attributes:
            - error_info (str): Error information.

        Usage example:
        try:
            # your code
        except AppointLimitException as e:
            print(f"{e.extra_info}")
    """

    def __init__(self, error_info):
        super().__init__(error_info)
        self.extra_info = error_info


def appoint_limit(limit: int, interval: int, pause: int):
    """
       The decorator that limits how often a function is called.

       :param:
       - limit (int): Maximum number of function call attempts in case of an error.
       - interval (int): The time interval in seconds during which function calls should be limited when working correctly.
       - pause (int): Pause between attempts to call a function in seconds in case of an error.

       :return:
       - wrapper (function): A wrapper function that limits how often the original function is called.

       Usage example №1:
       class MyClient:
            @appoint_limit(limit=5, interval=10, pause=2)
            async def my_method(self):
                # Your code for making an API request

       Usage example №2:
       @appoint_limit(limit=5, interval=10, pause=2)
       async def my_function():
           # your code
    """

    def decorator(func):
        last_call = datetime.min
        retries = 0

        @functools.wraps(func)
        async def wrapper(self, *args, **kwargs):
            nonlocal last_call, retries
            elapsed = datetime.now() - last_call
            if elapsed < timedelta(seconds=interval):
                await asyncio.sleep((timedelta(seconds=interval) - elapsed).total_seconds())
            last_call = datetime.now()
            try:
                return await func(self, *args, **kwargs)
            except Exception as e:
                if retries <= limit:
                    retries += 1
                    await asyncio.sleep(pause)
                    return await wrapper(self, *args, **kwargs)
                raise AppointLimitException(f"Script error: {e}\n In functions: {func.__name__}")

        return wrapper

    return decorator
